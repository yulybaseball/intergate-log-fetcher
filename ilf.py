

"""
IMPORTANT!!

NEVER run this script directly!

This script is intended to be run remotely.
It will fetch logs from the server it's intended to run on.
Script will print back a dictionary with the results of fetching logs.
"""

from os import path, remove

import subprocess
import zipfile

COMMAND_GREP_LIST = "cd {log_path}; grep -l {aid} *"
COMMAND_GREP_LIST_IDX = "cd {log_path}; grep -l {aid} *.idx*"
COMMAND_LIST_IDX = "cd {log_path}; ls -l | grep *.idx"
COMMAND_GREP_CONTENT = "cd {log_path}; grep {aid} {logfile}"

SEND_EMAIL_COMMAND = "uuencode {zip_path} {zip_name} | mailx -s \"{subject}\" {email}"
EMAIL_DOMAIN = "tracfone.com"

def _execute_command(command):
    """Execute command and return output."""
    p = subprocess.Popen(command, stdin=subprocess.PIPE, shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, dummy = p.communicate()
    return stdout.strip()

def _remove_duplicates(l):
    """Remove duplicated items in list 'l' and return it."""
    return list(set(l))

def _aid_on_files(aid, log_path, use_index_file=True):
    """Return a list of files where 'aid' is present."""
    idx = False
    if use_index_file:
        ls_command = _execute_command(COMMAND_LIST_IDX.format(log_path=log_path))
        if ls_command: #  log.idx file exists
            grep_command = COMMAND_GREP_LIST_IDX.format(aid=aid, log_path=log_path)
            idx = True
        else:
            return []
    else:
        grep_command = COMMAND_GREP_LIST.format(aid=aid, log_path=log_path)
    aidfiles_txt = _execute_command(grep_command)
    aidfiles_list = aidfiles_txt.split('\n') if aidfiles_txt else []
    if idx:
        #  Parse IDX files to get the actual log file containing the AID
        aid_files = []
        for aidfile in aidfiles_list:
            idx_content = _execute_command(COMMAND_GREP_CONTENT.format(
                                log_path=log_path, aid=aid, logfile=aidfile))
            idx_cnt_list = idx_content.split('\n') if idx_content else []
            for idx_entry in idx_cnt_list:
                #  every entry into the IDX file
                #  entry is supposed to be this way:
                #  <AID>,<complete_log_file_path>
                aid_files.append(idx_entry.split(',')[1])
        return _remove_duplicates(aid_files)
    return _remove_duplicates(aidfiles_list)

def _zip_files(log_path, grep_files_created, aid, counter=None):
    """Create a zip file for all grep-ed files in one directory.

    Keyword arguments:
    log_path -- complete path of the directory which the files were grep-ed in.
    grep_files_created -- list of files used to look for the AID (complete 
        path to the files, always inside 'log_path' directory).
    aid -- AID looked for.
    counter -- text containing information of how many files are already been 
        created for this server. It will be included into the final zip file 
        name.

    Return the name of the zip file (complete path).
    """
    if not grep_files_created:
        return None
    server_name = _execute_command("hostname")
    zipname = "{0}-{1}-{2}.zip".format(server_name, aid, counter)
    zipfn = path.join(log_path, zipname)
    zip_file = zipfile.ZipFile(zipfn, 'a')
    for grep_file in grep_files_created:
        try:
            zip_file.write(grep_file, compress_type=zipfile.ZIP_DEFLATED)
            remove(grep_file)
        except:
            pass
    zip_file.close()
    return zipfn

def _send_email(zip_file, aid, email_user):
    """Prepare the command for sending the email with the zip file attached."""
    zip_name = path.basename(zip_file)
    subject = "Your INTERGATE log request for AID={aid}".format(aid=aid)
    email_address = "{0}@{1}".format(email_user, EMAIL_DOMAIN)
    command = SEND_EMAIL_COMMAND.format(zip_path=zip_file, 
                                        zip_name=zip_name, 
                                        subject=subject, 
                                        email=email_address)
    _execute_command(command)

def fetch(conf_json_data):
    """Fetch AID log from paths defined in conf_json_data.

    Keyword arguments:
    conf_json_data -- dict containing the paths, AID and username to be used 
        for the search and for sending the logs by email. Must have these 
        keys:
        paths -- list of paths to search in.
        aid -- AID to look for.
        user -- user name to be used as the destinatary of the email for 
            sending the logs found.
    
    Return a dictionary of this form:
        {
            <log_path_1>: {
                [log_files]: [],
                [zip_files]: [],
                ['error']: error_message
            }
        }
    """
    result = {'log_files': [], 'zip_files': []}
    grep_files_created = []
    counter = 1
    use_index_file = conf_json_data['index']
    for log_path in conf_json_data['paths']:
        if path.isdir(log_path):
            aid = conf_json_data['aid']
            aid_files = _aid_on_files(aid, log_path, use_index_file)
            for aid_file in aid_files:
                aid_grep = _execute_command(COMMAND_GREP_CONTENT.format(
                                log_path=log_path, aid=aid, logfile=aid_file))
                if aid_grep:
                    content_file = "{0}.{1}".format(aid_file, aid)
                    try:
                        with open(content_file, "w+") as _cf:
                            _cf.write(aid_grep)
                        grep_files_created.append(content_file)
                        result['log_files'].append(aid_file)
                    except Exception:
                        pass
            zip_file = _zip_files(log_path, grep_files_created, aid, counter)
            if zip_file:
                _send_email(zip_file, aid, conf_json_data['user'])
                result['zip_files'].append(zip_file)
                counter += 1
                remove(zip_file)
    return result

# call main clean method
# parameter JSON_CONTENT should be added through source code to this file 
print(fetch(JSON_CONTENT))
