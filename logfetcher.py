#!/bin/env python

"""
Fetches Intergate logs by searching Actions Item Id into the log files.

Usage:

./logfetcher.py -c <carrier> -a <action_item_id> -l <log_type> -u <user>

    -e <environment>: Either tst or prod (case insensitive).
    -c <carrier>: Name of the carrier. It should match one the of the carrier 
        names defined in the conf file under ./conf folder.
    -a <action_item_id>: AID to look for.
    -l <log_type>: Type of the log: Integration, API or Callback.
    -u <user>: Tracfone's username. This will be used to send the logs by 
        email to this user.
    -i [ip]: only used as information and security purposes when this script 
        is used from a front-end.
    -x: If specified, the search will rely on index.idx files to find the 
        AID. Otherwise, a grep will be done inside the logs folder.

All parameters are mandatory.

Note: this script doesn't send the email with the logs. It relies completely 
on the commands uuencode and mailx for this purpose.
"""

import ast
import getopt
import json
from os import path, remove
import sys
import time

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from pssapi.cmdexe import cmd
from pssapi.logger import logger as logmod
from pssapi.utils import conf

logger = None

EXEC_DIR = path.dirname(path.realpath(sys.argv[0]))
LOG_FILE = path.join(EXEC_DIR, 'logs', 'logfetcher.log')
FETCHER_EXE_FILE = path.join(EXEC_DIR, 'ilf.py')
REMOTE_EXE_FILE = path.join(EXEC_DIR, 'tmp', '{time}-{server}-ilf.py')
CONF_FILE = path.join(EXEC_DIR, 'conf', 'conf.json')

OPTIONS = "e:c:a:l:u:i:x"

def _quit_on_error(error):
    """Log error and send it to the caller script."""
    logger.error(error)
    print(json.dumps({"error": error}))

def _init(params):
    """Initialize logger object."""
    global logger
    logger_fmtter = {
        'INFO': '%(asctime)s - %(process)d - %(message)s',
        'DEFAULT': '%(asctime)s - %(process)d - %(levelname)s - %(message)s'
    }
    logger = logmod.load_logger(log_file_name=LOG_FILE, cgi=True, **logger_fmtter)
    logger.info("Started search: {0}".format(params))

def _create_conf(options, conf_json):
    """Create and return a dict containing the servers, paths and AID for 
    the current search."""
    opts = {}
    for option in options:
        opts[option[0]] = option[1]
    carrier = opts['-c']
    log_type = opts['-l']
    environment = opts['-e'].lower()
    servers = conf_json[environment][carrier][log_type]['servers']
    paths = conf_json[environment][carrier][log_type]['paths']
    final_conf = {}
    final_conf['servers'] = servers
    final_conf['paths'] = paths
    final_conf['aid'] = opts['-a']
    final_conf['email'] = opts['-u']
    final_conf['index'] = True if '-x' in opts else False
    return final_conf

def _create_tmp_script(server):
    """Create the temp file using server param and current time.
    Return the file name (complete path).
    """
    _file = None
    current_time = time.time()
    tmp_exe_file = REMOTE_EXE_FILE.format(time=current_time, server=server)
    try:
        _file = open(tmp_exe_file, 'w+')
    except Exception as e:
        logger.error(("Error creating temp script '{0}': {1}").\
            format(tmp_exe_file, str(e)))
        return False
    finally:
        if _file:
            _file.close()
    return tmp_exe_file

def _add_cont2remote_script(json_content, tmp_file):
    """Copy json_content to tmp_file."""
    json_content = "JSON_CONTENT = {0}".format(json_content)
    try:
        with open(FETCHER_EXE_FILE) as fetcherfile:
            with open(tmp_file, 'w') as tmp_f:
                tmp_f.write(json_content)
                for line in fetcherfile:
                    tmp_f.write(line)
    except Exception as e:
        logger.error("Error copying content to temp script: {0}".format(str(e)))
        return False
    return True

def _fetch(serv_conf):
    """Connect to servers and execute the python script to fecth logs."""
    servers = serv_conf['servers']
    if not servers:
        warntxt = "No SERVERS and/or PATHS are defined for selected carrier."
        logger.warning(warntxt)
        return {
            "warning": warntxt,
            "description": 
                "Please contact the owner of this application if you " + 
                "think the selected carrier should have a configuration " + 
                "defined for DB2Q/Q2DB/API/CALLBACK."
        }
    result = {}
    remote_json_content = {}
    remote_json_content['aid'] = serv_conf['aid']
    remote_json_content['paths'] = serv_conf['paths']
    remote_json_content['user'] = serv_conf['email']
    remote_json_content['index'] = serv_conf['index']
    for server in servers:
        result[server] = {}
        tmp_rem_file = _create_tmp_script(server)
        if tmp_rem_file and _add_cont2remote_script(remote_json_content, 
                                                    tmp_rem_file):
            logger.info("{0} - Temporal file was created: {1}".format(server,
                                                                tmp_rem_file))
            try:
                stdout, stderr = cmd.pythonexec('weblogic', server, 
                                                tmp_rem_file, True)
                if stderr:
                    logger.error("{0} - {1}".format(server, stderr.strip()))
                    result[server]['error'] = stderr
                if stdout and stdout != '\n':
                    logger.info("{0} - {1}".format(server, stdout.strip()))
                    result[server] = stdout.strip()
            except Exception as e:
                err = str(e)
                logger.error("{0} - {1}".format(server, err))
                result[server]['error'] = err
            finally:
                remove(tmp_rem_file)
                if not path.isfile(tmp_rem_file):
                    logger.info(("{0} - Temporal file was removed: {1}").\
                        format(server, tmp_rem_file))
                else:
                    logger.warning(
                        ("{0} - Temporal file couldn't be removed: {1}").\
                        format(server, tmp_rem_file))
        else:
            result[server]['error'] = "Error creating temp script."
    return result

def _convert_message2dict(message):
    """Convert message to a dict and return it."""
    result = {}
    if 'error' in message or 'warning' in message:
        return message
    for server in message:
        result[server] = message[server]
        server_value_dict = ast.literal_eval(message[server])
        result[server] = server_value_dict
    return result

def _main():
    fullparams = sys.argv
    params = fullparams[1:]
    _init(params)
    try:
        options, dummy = getopt.gnu_getopt(params, OPTIONS)
        conf_json = conf.get_conf(CONF_FILE)
        final_conf = _create_conf(options, conf_json)
        message = _fetch(final_conf)
        print(json.dumps(_convert_message2dict(message)))
    except Exception as e:
        _quit_on_error(str(e))

if __name__ == "__main__":
    _main()
